package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/vincent-blee/go-rest-test/src/routes"
)

/*
main is defined to be the entry point of the program.
It's the function that will be called by the runtime environment when the program is launched.
*/
func main() {
	// router is the default Engine instance.
	router := gin.Default()
	// Define Player API's routes.
	routes.RegisterPlayerRoutes(router)
	// Need to explore ServeMux documentation.
	http.Handle("/", router)
	// Listen port 8080 on TCP / IP stack and handle requests.
	http.ListenAndServe("localhost:8080", router)
}
