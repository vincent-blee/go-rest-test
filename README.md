# Go - REST TEST
> Go, Gorm, Gin, VueJS

First steps with GO to create an API REST.
This project aim to help to learn and use a new stack.

## Prerequisites

Go 1.17
MySql or MariaDB


## Getting started

```shell
cd app && go run main.go
```

## Use API

| Use case | URL | METHOD | PARAM |
| ------ | ------ | ------ | ------ |
| Fetch all players | http://localhost:8080/players | GET | |
| Get player by ID | http://localhost:8080/players/player/{:id} | GET | |
| Create player | http://localhost:8080/players | POST | FirstName _string_, LastName _string_, Birthday _time.Time_ |
| Update player | http://localhost:8080/players/player/{:id} | PUT | FirstName _string_ \| LastName _string_ \| Birthday _time.Time_ |
| Delete player | http://localhost:8080/players/player/{:id} | DELETE | |

Relace {:id} by a unit.


## Build

```shell
cd app && go build
```

## Features

* Player API
