package config

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

/*
@ title Connect
@ description Initialize a new connection with the database.
@ return *gorm.DB
*/
func Connect() *gorm.DB {
	// Define the data source name.
	dsn := "vincent:password_test@tcp(127.0.0.1:3306)/noughts_n_crosses?charset=utf8mb4&parseTime=True&loc=Local"
	// Open initialize db session based on mysql dialector.
	d, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	// If an error occurred, stop the goroutine.
	if err != nil {
		panic(err)
	}
	// db global var take the DB session.
	return d
}
