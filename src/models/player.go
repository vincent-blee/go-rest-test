package models

import (
	"time"

	"gitlab.com/vincent-blee/go-rest-test/src/config"
	"gorm.io/gorm"
)

var db *gorm.DB

/*
Define Player type.
Gorm model includes :
- DeletedAt (soft delete). When calling Delete, just set DeleteAt's value to the current time.
- UpdatedAt. When record is updated, UpdatedAt's value is set to the current time.
- CreatedAt. When a new record is created, CreatedAt is set to the current time.
*/
type Player struct {
	gorm.Model
	FirstName string
	LastName  string
	Birthday  time.Time
}

/*
@ title init
@ description The init func initalize a new connection,
stock a DB Gorm object into RAM and create the table if needed.
*/
func init() {
	// Module config establish a new connection the the database.
	db = config.Connect()
	// Use the DB session to create Player table if doesn't exist.
	db.AutoMigrate(&Player{})
}

/*
@ title CreateTable
@ description Create a new player a save a new record.
@ param Player *Player A pointer address.
@ return err error|nil
*/
func CreatePlayer(Player *Player) (err error) {
	// Use method chaining to create new player's record and get the error field.
	err = db.Create(Player).Error
	// If error occured, return it.
	if err != nil {
		return err
	}
	// If no error, return nil.
	return nil
}

/*
@ title GetAllPlayers
@ description Fetch all players recorded.
@ param Player *[]Player A pointer address of an array of Player.
@ return err error|nil
*/
func GetAllPlayers(Player *[]Player) (err error) {
	// Use method chaining to find all players recorded and get the error field.
	// Here, Find change the player value unsing the memory address of player.
	err = db.Find(&Player).Error
	// If error occured, return it.
	if err != nil {
		return err
	}
	// If no error, return nil.
	return nil
}

/*
@ title GetPlayerById
@ description Find a recorded player by his id.
@ param Player *Player A pointer address.
@ param ID unit64 The player's ID.
@ return err error|nil
*/
func GetPlayerById(Player *Player, ID uint64) (err error) {
	// Use method chaining to get first record where ID eq the ID param and get the error field.
	// Here, First change the player value unsing the memory address of player.
	err = db.Where("ID = ?", ID).First(&Player).Error
	// If error occured, return it.
	if err != nil {
		return err
	}
	// If no error, return nil.
	return nil
}

/*
@ title UpdatePlayer
@ description Update a recorded player.
@ param Player *Player A pointer address.
@ return err error|nil
*/
func UpdatePlayer(Player *Player) (err error) {
	// Use method chaining to save the new player and get the error field.
	err = db.Save(Player).Error
	// If error occured, return it.
	if err != nil {
		return err
	}
	// If no error, return nil.
	return nil
}

/*
@ title DeletePlayer
@ description Update a recorded player.
@ param Player *Player A pointer address.
@ param ID unit64 The player's ID.
@ return err error|nil
*/
func DeletePlayer(Player *Player, ID uint64) (err error) {
	// Use method chaining to delete the record where ID eq the ID param and get the error field.
	err = db.Where("ID = ?", ID).Delete(Player).Error
	// If error occured, return it.
	if err != nil {
		return err
	}
	// If no error, return nil.
	return nil
}
