package controllers

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/vincent-blee/go-rest-test/src/models"
)

/*
@ title CreatePlayer
@ description Create a new player record.
@ param c *gin.Context The Gin context.
@see https://pkg.go.dev/github.com/gin-gonic/gin#Context
*/
func CreatePlayer(c *gin.Context) {
	// Define a new Player.
	var player models.Player
	// Save JSON data into the destination player (ram).
	c.BindJSON(&player)
	// Calls CreatePlayer method from models module with player value memory address.
	err := models.CreatePlayer(&player)
	if err != nil {
		// If the record failed, aboarts transaction with a server error status code and return the error message.
		c.AbortWithStatusJSON(500, gin.H{"error": err})
		return
	}
	// Sends a JSON containing a message and the recorded player data.
	c.JSON(200, gin.H{
		"message": "OK: new player has been created.",
		"data":    player,
	})
}

/*
@ title GetPlayers
@ description Fetch all players.
@ param c *gin.Context The Gin context.
@see https://pkg.go.dev/github.com/gin-gonic/gin#Context
*/
func GetPlayers(c *gin.Context) {
	// Define a new array of Players.
	var players []models.Player
	// Calls GetAllPlayers method from models module with player value memory address.
	err := models.GetAllPlayers(&players)
	if err != nil {
		// If the record failed, aboarts transaction with a server error status code and return the error message.
		c.AbortWithStatusJSON(500, gin.H{"error": err})
		return
	}
	// Sends a JSON containing a message and the recorded player data.
	c.JSON(200, gin.H{
		"message": "OK: the list of players has been transmitted",
		"data":    players,
	})
}

/*
@ title GetPlayer
@ description Find player by ID.
@ param c *gin.Context The Gin context.
@see https://pkg.go.dev/github.com/gin-gonic/gin#Context
*/
func GetPlayer(c *gin.Context) {
	// Need this to convert the ID param string into uint64 to match with the model structure.
	id, err := strconv.ParseUint(c.Params.ByName("id"), 10, 0)
	if err != nil {
		panic(err.Error())
	}
	// Define a new array of Players.
	var player models.Player
	// Calls GetPlayerById method from models module with player value memory address and the player ID.
	err = models.GetPlayerById(&player, id)
	if err != nil {
		// If the record failed, aboarts transaction with a server error status code and return the error message.
		c.AbortWithStatusJSON(500, gin.H{"error": err})
		return
	}
	// Sends a JSON containing a message and the recorded player data.
	c.JSON(200, gin.H{
		"message": "OK: the player's informations has been transmitted",
		"data":    player,
	})

}

/*
@ title UpdatePlayer
@ description Update a player record.
@ param c *gin.Context The Gin context.
@see https://pkg.go.dev/github.com/gin-gonic/gin#Context
*/
func UpdatePlayer(c *gin.Context) {
	// Need this to convert the ID param string into uint64 to match with the model structure.
	id, err := strconv.ParseUint(c.Params.ByName("id"), 10, 0)
	if err != nil {
		panic(err.Error())
	}
	// Define a new array of Players.
	var player models.Player
	// Calls GetPlayerById method from models module with player value memory address and the player ID.
	err = models.GetPlayerById(&player, id)
	if err != nil {
		// If the record failed, aboarts transaction with a server error status code and return the error message.
		c.AbortWithStatusJSON(500, gin.H{"error": err})
		return
	}
	// Save JSON data into the destination player (ram).
	c.BindJSON(&player)
	// Calls UpdatePlayer method from models module with player value memory address.
	err = models.UpdatePlayer(&player)
	if err != nil {
		// If the record failed, aboarts transaction with a server error status code and return the error message.
		c.AbortWithStatusJSON(500, gin.H{"error": err})
		return
	}
	// Sends a JSON containing a message and the recorded player data.
	c.JSON(200, gin.H{
		"message": "OK: the player's informations has been updated.",
		"data":    player,
	})

}

/*
@ title DeletePlayer
@ description Delete a player record.
@ param c *gin.Context The Gin context.
@see https://pkg.go.dev/github.com/gin-gonic/gin#Context
*/
func DeletePlayer(c *gin.Context) {
	// Need this to convert the ID param string into uint64 to match with the model structure.
	id, err := strconv.ParseUint(c.Params.ByName("id"), 10, 0)
	if err != nil {
		panic(err.Error())
	}
	// Define a new array of Players.
	var player models.Player
	// Calls DeletePlayer method from models module with player value memory address and the player ID.
	err = models.DeletePlayer(&player, id)
	if err != nil {
		// If the record failed, aboarts transaction with a server error status code and return the error message.
		c.AbortWithStatusJSON(500, gin.H{"error": err})
		return
	}
	// Sends a JSON containing a message and the recorded player data.
	c.JSON(200, gin.H{
		"message": "OK: the player is now deleted.",
	})
}
