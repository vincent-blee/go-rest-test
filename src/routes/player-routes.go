package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/vincent-blee/go-rest-test/src/controllers"
)

// Define Player API's routes and handlers for each protocols.
var RegisterPlayerRoutes = func(router *gin.Engine) {
	router.GET("/players", controllers.GetPlayers)
	router.POST("/players", controllers.CreatePlayer)
	router.GET("/players/player/:id", controllers.GetPlayer)
	router.PUT("/players/player/:id", controllers.UpdatePlayer)
	router.DELETE("/players/player/:id", controllers.DeletePlayer)
}
